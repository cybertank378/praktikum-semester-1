#include  <iostream>
#include  <stdlib.h>//definisi random,randomize
using namespace std;

void genBilangan(int number);
int nilaiAcak();

int main(){
	int a;
	string cbLagi;
	do {
		system("CLS");
		cout << "Berapa banyak random number ? ";
		cin >> a;
		cout << "\n\n";
		genBilangan(a);
		cout << "coba lagi (y/t)";
		cin >> cbLagi;
	
	} while (cbLagi == "y" || cbLagi == "Y");

	cout << "\n\n";
	cout << "Terimakasih sudah menggunakan program ini.\n\n" ;
	return 0;
}

void genBilangan(int number) {
	int bil[100], b, ganjil = 0, genap = 0;
	for (b = 1; b <= number; b++) {
		//menghasilkan bilangan acak antara 0-100
		bil[b] = nilaiAcak();
		//cek jika nilai yang ada pada bil[b] habis di modulos 2 maka genap
		if (bil[b] % 2 != 0) {
			ganjil++;
			cout << bil[b] << "	Bilangan ganjil" << endl;
		} else {
			genap++;
			cout << bil[b] << "	Bilangan genap" << endl;
		}
	}
	cout <<"total bilangan genap ada  = "<< genap << endl;
	cout <<"total bilangan ganjil ada = " << ganjil << endl << endl;
}

int nilaiAcak() {
	return rand() % 100 + 1;
}