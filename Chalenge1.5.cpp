#include<iostream>
#include<string>

using namespace std;

void inputKm(); // Deklarasi Modul Input Kilometer
int hitung(int km);

int  main() {

	inputKm(); //Panggil Modul Input Kilometer

	cout << "\n\n";
	return 0;
}

void inputKm() {
	int km;

	cout << "input jarak dalam kilometer ?";
	cin >> km;

	cout << "konversi jarak dalam miles adalah? " << hitung(km) << " Mil";
}

int hitung(int km) {
	int miles;
	float nilaiKonversi = 0.6214;

	miles = km * nilaiKonversi;
	return miles;
}