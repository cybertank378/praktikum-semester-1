#include <iostream>
#include <string> 
#include <conio.h>

using namespace std;

int main(){
    int i,counter, jmlMhs = 0;
    float total, rata[100], nilai;
    char status;
    string namaMhs;

    do {
        cout << "Masukkan jumlah mahasiswa ? ";
        cin >> jmlMhs;
        cout << endl;

        for (i = 1; i <= jmlMhs; i++) {
            total = 0;
            cout << "Masukkan nama mahasiswa ke - " << i << " ? ";
            //getline(cin, namaMhs);
            cin >> namaMhs;
            for (counter = 1; counter <= 3; counter++) {
                cout << "->Nilai mahasiswa ke-" << counter << " = ";
                cin >> nilai;
                total += nilai;
            }

            rata[i] = total / 3.0f;
            cout << endl;
            cout << "Nilai Rata-rata = " << rata[i] << endl;
            cout << "--------------------------" << endl;
        }

        cout << "Coba lagi <Y/T> ? ";
        cin >> status;

    } while (status == 'Y' || status == 'y');

    cout << "Terima kasih sudah menggunakan program ini" << endl;
    cin.ignore(100000, '\n');
    return _getch();
}