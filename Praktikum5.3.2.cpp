#include  <iostream>
#include  <stdlib.h>//definisi random,randomize
using namespace std;

bool isInvalid(int model);

int main() {
	int model;
	cout << "Input model number ? ";
	cin >> model;
	while (isInvalid(model)) {
		cout << "ERROR : the valid model number are 100, 200 and 300.\n ";
		cout << "Input a valid model number ? ";
		cin >> model;
	}
	cout << "\nValid model! thank you...\n\n ";
	return 0;
}

bool isInvalid(int model) {
	bool status;
	if (model != 100 && model != 200 && model != 300) {
		status = true;
	}else {
		status = false;
	}
	return status;
}

