#include<iostream>
#include<string>

using namespace std;

void showBulan(int number); // Deklarasi Bulan

int  main() {
	int number;
	cout << "Masukkan sebuah angka ?";
	cin >> number;

	showBulan(number);  // tampilkan bulan 
	
	return 0;
}

void showBulan(int number) {
	switch (number) {
	case 1:
		cout << number << " = Januari";
		break;
	case 2:
		cout << number << " = Februari";
		break;
	case 3:
		cout << number << " = Maret";
		break;
	default:
		cout << "Invalid Number...";
	}
	cout << "\n\n";
}