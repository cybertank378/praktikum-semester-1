#include <iostream>
#include <string> 
#include <conio.h>

using namespace std;

int  main() {
	double number, power;
	string cbLagi;

	do {
		system("CLS");

		cout << "Input a number ? "; cin >> number;
		cout << "Input power value ? "; cin >> power;
		cout << "\nValue " << number << " ^ " << power << " = " << pow(number, power);
		cout << "\n\nCoba lagi (y/t) ? ";
		cin >> cbLagi;
	} while (cbLagi == "y");
	cout << "\n\n";
	return 0;
}



