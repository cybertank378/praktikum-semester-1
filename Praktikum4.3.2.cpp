#include <iostream>
#include <string> 
#include <conio.h>

using namespace std;

void inputData();
int luasPJ(int p, int j);

int  main() {
	string cbLagi;
	do {
		system("CLS");
		inputData();
		cout << "\n\nCoba Lagi (y/t) ?";
		cin >> cbLagi;
	} while (cbLagi == "y");

	cout << "\n\n";
	return 0;
}

void inputData() {
	int panjang, lebar, luas;
	cout << "Menghitung luas persegi panjang\n";
	cout << "-------------------------------\n";
	cout << "Input panjang ? ";
	cin >> panjang;
	cout << "Input lebar ? ";
	cin >> lebar;
	cout << "\nLuas persegi panjang = " << luasPJ(panjang, lebar);
}

int luasPJ(int p, int l) {
	return (p * l);
}

