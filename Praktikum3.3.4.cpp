#include<iostream>
#include<string>

using namespace std;

int  main() {
	int space = 9;

	for (int x = 1; x <= 9;x++) {
		for (int s = 1; s <= space; s++) {
			cout << " ";
		};

		for (int y = 1; y <= x; y++) {
			cout <<  y << " ";
		};

		cout << "\n\n";
		space--;
	}

	cout << "\n\n";
	return 0;
}